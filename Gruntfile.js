module.exports = function(grunt) {

  grunt.initConfig({
      bowercopy: {
          options: {
          },
	  css: {
	     options: {
                 destPrefix: 'app/css'
             },
	     files: {
		'ionic/release/css/ionic.min.css': 'ionic/release/css/ionic.min.css',
		'ionic/release/fonts/ionicons.woff': 'ionic/release/fonts/ionicons.woff',
		'ionic/release/fonts/ionicons.svg': 'ionic/release/fonts/ionicons.svg',
		'ionic/release/fonts/ionicons.eot': 'ionic/release/fonts/ionicons.eot',
		'ionic/release/fonts/ionicons.ttf': 'ionic/release/fonts/ionicons.ttf',
		'angular-loading-bar/build/loading-bar.min.css': 'angular-loading-bar/build/loading-bar.min.css',
		'jquery-ui/themes/ui-lightness/jquery-ui.min.css': 'jquery-ui/themes/ui-lightness/jquery-ui.min.css',
	     }
	  },
	  js:{
	     options: {
                 destPrefix: 'app/js'
             },
	     files: {
		'jquery/dist/jquery.min.js': 'jquery/dist/jquery.min.js',
		'jquery-ui/jquery-ui.min.js': 'jquery-ui/jquery-ui.min.js',
		'angular/angular.min.js' : 'angular/angular.min.js',
		'angular-animate/angular-animate.min.js': 'angular-animate/angular-animate.min.js',
		'angular-sanitize/angular-sanitize.min.js': 'angular-sanitize/angular-sanitize.min.js',
		'angular-ui-router/release/angular-ui-router.min.js': 'angular-ui-router/release/angular-ui-router.min.js',
		'angular-cache/dist/angular-cache.min.js': 'angular-cache/dist/angular-cache.min.js',
		'angular-resource/angular-resource.min.js': 'angular-resource/angular-resource.min.js',
		'ngCordova/dist/ng-cordova.min.js': 'ngCordova/dist/ng-cordova.min.js',
		'angular-sanitize/angular-sanitize.min.js': 'angular-sanitize/angular-sanitize.min.js',
		'angular-loading-bar/build/loading-bar.min.js': 'angular-loading-bar/build/loading-bar.min.js',
		'cryptojslib/rollups/sha512.js': 'cryptojslib/rollups/sha512.js',
		'collide/collide.js': 'collide/collide.js',
		'ionic/release/js/ionic.min.js': 'ionic/release/js/ionic.min.js',
		'ionic/release/js/ionic-angular.min.js': 'ionic/release/js/ionic-angular.min.js',
		'moment/min/moment.min.js': 'moment/min/moment.min.js',
		'angular-touch/angular-touch.min.js': 'angular-touch/angular-touch.min.js'
 	     }
          }
      },
      bump: {
            options: {
                files: [
                    "app/bower.json",
		    "app/config.xml",
                    "bower.json",
		    "package.json"
                ],
                commit: false,
                commitMessage: 'chore(release): v%VERSION%',
                commitFiles: [
                    "app/bower.json",
		    "app/config.xml",
                    "bower.json",
		    "package.json"
                ],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                pushTo: 'origin'
            }
        },
      
         /**
         * Creates a changelog on a new version.
         */
        changelog: {
            options: {
                dest: 'CHANGELOG.md',
                template: 'changelog.tpl'
            }
        }

  });
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-bump-cordova');
  grunt.loadNpmTasks('grunt-conventional-changelog');
};
