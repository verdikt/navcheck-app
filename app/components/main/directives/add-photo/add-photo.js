'use strict';
app.directive('addPhoto', function($ionicActionSheet, $timeout, $q){

	return {
		templateUrl: 'components/main/directives/add-photo/add-photo.html',
		restrict: 'E',
		scope: {
			item: '='
		},
		link: function(scope, elem){
		
			var resizeImage = function(base64, width) {
				var defer = $q.defer();
				/*var canvas = document.createElement('canvas'),
					ctx = canvas.getContext('2d'),
					img = new Image;
					
				canvas.width = width;
				
				
				img.onload = function(){
					canvas.height = img.height * (width/img.width);
					ctx.drawImage(img, 0, 0, width, img.height * (width/img.width));
					defer.resolve(canvas.toDataURL());
				}
				img.src = 'data:image/jpg;base64,' + base64;
				*/
				defer.resolve(base64);
				return defer.promise;
			}
			scope.$watch('item', function(item){
				if(item && !item.pics){
					item.pics = [];
				}
			});
			
			scope.deletePic = function(index){
				scope.item.pics.splice(index, 1);
			};
			elem.find('button').bind('click', function(){
				$ionicActionSheet.show({
					buttons: [
						{ text: 'Capture Photo'},
						{ text: 'From Photo Library' },
						{ text: 'From Photo Album' }
					],
					titleText: 'Select a source',
					cancelText: 'Cancel',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						var getPhoto = function (source) {
							navigator.camera.getPicture(function(imgData){
								resizeImage(imgData, 400)
								.then(function(imgResized){
									scope.item.pics.push(imgResized);
									//scope.$apply();
								})
							}, function(e){
								console.warn(e);
							}, { 
								quality: 60,
								allowEdit: true,
								destinationType: navigator.camera.DestinationType.DATA_URL,
								sourceType: source 
							});
						};
						if(!navigator.camera){
							scope.item.pics.push('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');//transparent img if no camera (ie chrome)
							return true;
						}
						switch(index){
							case 0:
								getPhoto(navigator.camera.PictureSourceType.CAMERA);
							break;
							case 1:
								getPhoto(navigator.camera.PictureSourceType.PHOTOLIBRARY);
							break;
							case 2:
								getPhoto(navigator.camera.PictureSourceType.SAVEDPHOTOALBUM);
							break;
						}
						return true;
					}
				});
				scope.$apply();
			});
		}
	}
});

