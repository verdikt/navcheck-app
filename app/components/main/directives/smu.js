'use strict';
app.directive('smu', function(){
    return {
        template: '<span>{{smu}}</span>',
        restrict: 'E',
        scope: {
            unit: '=',
            value: '=',
            optional: '='
        },
        link: function(scope){
            switch(scope.unit){
                case '1':
                    scope.smu = scope.value + ' Hour Meter Reading (whole hours)';
                    scope.optional = false;
                    break;
                case '2':
                    scope.smu = scope.value + ' Odometer Reading (whole kilometers)';
                    scope.optional = false;
                    break;
                case '3':
                    scope.smu = scope.value + ' SMU (Service Meter Units)';
                    scope.optional = true;
                    break;
            }
        }
    }
});

app.directive('adaptive', function($window){
    return {
        restrict: 'A',
        scope: {
            hor: '=',
            ver: '='
        },
        link: function(scope, elem) {

            var updateOrientation = function() {
                if( Math.abs($window.orientation % 180) == 0 ){
                    $(elem).removeClass(scope.hor);
                    $(elem).addClass(scope.ver);
                }else{
                    $(elem).removeClass(scope.ver);
                    $(elem).addClass(scope.hor);
                }
            };
            $window.addEventListener("orientationchange", updateOrientation);
            updateOrientation();
        }
    }
});