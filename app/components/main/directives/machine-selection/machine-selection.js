app.directive('machineSelection', function($state, $stateParams, $filter, $ionicScrollDelegate, DSCacheFactory, $rootScope){
	 var $cache = DSCacheFactory.get($rootScope.hash);

	 return {
		restrict: 'E',
		templateUrl: 'components/main/directives/machine-selection/machine-selection.html',
		scope:{
			siteMachines: '=',
			allMachines: '=',
			param: '=',
			back: '=',
			next: '=',
			confirm: '=',
			site: '='
		},
		link: function(scope){
		
			scope.findMachine = function(machine){
				var machineNumber = scope.param.machineNumber? scope.param.machineNumber.toLowerCase().replace(/^[0]+/g,""): '';
				return ( 
					(machine.NavaceNo && machine.NavaceNo.toLowerCase().indexOf(machineNumber) !== -1 ) ||
					(machine.PlantNo && machine.PlantNo.toLowerCase().indexOf(machineNumber) !== -1 ) ||
					(machine.RegistrationNumber && machine.RegistrationNumber.toLowerCase().indexOf(machineNumber) !== -1 )
				);
			}
			scope.confirmation = false;
			scope.change = false;
			
			scope.$watch('confirmation', function(){
				$ionicScrollDelegate.resize();
			})
			scope.clearMachine = function(){
				scope.param.machine = null;
			}
			scope.reload = function(){
				$state.transitionTo($state.current, $stateParams, {reload: true, inherit: false, notify: true});
			}
			scope.machines = {
				onSite: [],
				offSite: []
			};
			var paramMasterSiteId = scope.param.site ? scope.param.site.MasterSiteId : 0;
			var siteMachines = $filter('unique')($filter('filter')(scope.siteMachines, function(siteMachine){ 
									return siteMachine.MasterSiteId == paramMasterSiteId
								}));
			angular.forEach(scope.allMachines, function(machine){
				var machineOnSite = false;
				angular.forEach(siteMachines, function(siteMachine){
					if(siteMachine.MachineId.toString() == machine.MachineId.toString()){
						machineOnSite = true;
						scope.machines.onSite.push(machine);
					}
				});
				if(!machineOnSite){
					scope.machines.offSite.push(machine);
				}
			});
			scope.select = function(machine){
				scope.param.machineNumber = null;
				scope.param.machine = machine;
				scope.goNext();
			}

			
			scope.goNext = function(){
				scope.confirmation = false;
				$cache.put('defaultMachine', scope.param.machine);
				$state.go(scope.next);
			}
		}
    };
});
app.directive('machineItem', function(){
	 return {
		templateUrl: 'components/main/directives/machine-selection/machine-item.html',
		restrict: 'E',
		scope:{
			machine: '=',
			filter: '='
		},
		link: function(scope){
			scope.$watch('filter', function(val){
				if(!val){
					return '';
				}
				scope.paramFilter = val.toLowerCase().replace(/^[0]+/g,'');
			});
		}
	}
});