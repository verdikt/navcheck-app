app.directive('blur', function($timeout){
	 return {
		restrict: 'A',
		link: function(scope, elem, attr){
			var el = $(elem);
			var anim = function(){
				el.css({
					'-webkit-filter': 'blur(15px) grayscale(1)',
					'transition': 'all 0.5s'
				});
				$timeout(function(){
					el.css({'-webkit-filter': 'blur(0) grayscale(0)'});
				}, 300);
			}
			el.click(anim);
			anim();
		}
    };
});

app.directive('check', function($timeout){
	 return {
		restrict: 'A',
		link: function(scope, elem, attr){
			var check = function(e){
				var target = $(e.target),
				parent = target.parent('.item-input'),
				classes = target.attr('class');
				if(classes && classes.indexOf('ng-valid') > -1 && eval('scope.'+target.attr('ng-model')) &&  (target.html() != '' || target.val() != '')){
					parent.addClass('my-ng-valid');
				}else{
					console.log(classes)
					parent.removeClass('my-ng-valid');
				}
			},
			el = $(elem);
			el.bind('blur, change', check);
			$timeout(function(){
				el.find('.ng-pristine').each(function(index, ui){
					check({target: ui});
				});	
			}, 150);
			
		}
    };
});

app.directive('button', function($ionicNavBarDelegate){
	 return {
		restrict: 'E',
		link: function(scope, elem, attr){
			var but = $(elem);
			if(but.html().toLowerCase() == 'back'){
				but.addClass('icon-left ion-ios7-arrow-back');
				but.click(function(e){
					if(but.attr('no-override') != null){
						return;
					}
					e.preventDefault();
					$ionicNavBarDelegate.back();
					return false;
				});
			}
			if(but.html().toLowerCase() == 'next'){
				but.addClass('icon-right ion-ios7-arrow-forward');
			}
			
			
		}
    };
});
app.directive('fullBlock', function($timeout){
	 return {
		restrict: 'A',
        priority: 1001,
		link: function(scope, elem, attr){
           
            var align = function(){
                var left = $(elem).offset().left;
                if(left == 0){ return;}
                $(elem).css({
                        'position': 'relative', 
                        'left': -left +'px', 
                        width: $(window).width(), 
                        padding: 0, 
                        margin: 0
                });
            };
            align();
            //$timeout(align, 200);
            
		}
    };
});



