app.directive('siteSelection', function($state, $stateParams, DSCacheFactory, $state, $rootScope){
	 var $cache = DSCacheFactory.get($rootScope.hash);
	 return {
		transclude: true,
		restrict: 'E',
		templateUrl: 'components/main/directives/site-selection/site-selection.html',
		scope:{
			sites: '=',
			param: '=',
			back: '=',
			next: '=',
		},
		link: function(scope){
			scope.change = false;
			scope.select = function(site){
				scope.param.site = site;
				$cache.put('defaultSite', site);
				$state.go(scope.next);
			}
			scope.goBack = function(){
				$state.go(scope.back);
			}
			
			scope.state = $state;
			
			scope.reload = function(){
				$state.transitionTo($state.current, $stateParams, {reload: true, inherit: false, notify: true});
			}
		}
    };
});
 