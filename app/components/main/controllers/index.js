app.controller('MainCtrl', function(Operator, $rootScope, $state, $scope, $timeout, $ionicPopup, $interval){
    
    if($rootScope.hash == null){
        $state.go('splash');
    }else{
        $rootScope.operator = Operator.get().then(function(obj){
            $rootScope.operator = obj;
        }, function(){
            $state.go('splash');
        });
    }
	//window.location.reload(true);
	$rootScope.status = { 1: 'OK', 2: 'Adjusted', '3': 'Replaced', 4: 'Requires repair (Not Urgent)', 5: 'Environmental Issue', 6: 'Safety Issue', 7: 'Reported' };
	$rootScope.Math = window.Math;
	$rootScope.moment = moment;

    $rootScope.gps = true;
    $interval(function(){
        navigator.geolocation.getCurrentPosition(function(){
                $rootScope.gps = true;
            },
            function(){
                if($rootScope.gps == true){
                    var alertPopup = $ionicPopup.alert({
                        title: 'Geo Location',
                        template: 'Unable to get your location! Please check your settings.',
                        buttons: [{
                            text: 'OK',
                            type: 'button-danger'
                        }]
                    });
                }
                $rootScope.gps = false;
            },
            { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true });
    }, 10000)


});