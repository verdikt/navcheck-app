 'use strict';
app.factory('Checksheet', function(Api, DSCacheFactory, $q, $rootScope, $interval, $filter, Settings){
	var $checksheetService = Api({cache: false, type: 'checksheet'});
	var inProgress = false;

	var $checksheet = {
		add: function(checksheet){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstChecksheets');
			
			if(!checksheet.timestamp){
				checksheet.timestamp = Date.now();
			}
			if(!lst){ lst = []; }
			checksheet.id = Math.uuid();
			(lst)?lst.push(checksheet):lst = [checksheet];
			DSCacheFactory.get($rootScope.hash).put('lstChecksheets', lst);
			defer.resolve();
			
			return defer.promise;
		},
		get: function(filter){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstChecksheets');
			if(filter){
				lst = $filter('filter')(lst, filter);
			}
			if(!lst || lst.length == 0){
				defer.reject([]);
				
			}else{
				defer.resolve(lst);
			}
			return defer.promise;
		},
		destroy: function(){
			DSCacheFactory.get($rootScope.hash).remove('lstChecksheets');
		},
		save: function(){
			var defer = $q.defer();
			$checksheet
			.get(function(elem){
					return !elem.submitted;
			 })
			.then(function(lst){

				if(lst.length==0 || inProgress){ defer.resolve();return; }
				$checksheetService.save({
					checksheets: lst
				}, 
				function(obj){

						$checksheet
						.get()
						.then(function(lst2){
                            angular.forEach(lst, function(elem){
                                angular.forEach(lst2, function(elem2){
                                    if(elem.id == elem2.id){
                                        elem2.submitted = true;
                                    }
                                });
						     });
                            inProgress = false;
				            DSCacheFactory.get($rootScope.hash).put('lstChecksheets', lst2);
							defer.resolve(obj);
					});
				}, 
				function(obj){
					console.warn('Unable to send checksheet.', obj);
					inProgress = false;
					defer.reject(obj);
				});
			});
				
			return defer.promise;
		}
	}
	var interval = $interval(function(){
		$checksheet.save();
	}, Settings.interval);
	$checksheet.interval = interval;
	return $checksheet;
});

