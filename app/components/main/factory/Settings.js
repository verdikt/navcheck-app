app.factory('Settings', function($rootScope, DSCacheFactory){

    $rootScope.version = '5.0.21';

	$rootScope.updated = '14/05/15';
	return {
		version: $rootScope.version,
		apiEndpoint: 'https://navaceapi.verdikt.com.au/api/',
		//apiEndpoint: 'https://dev.api.navace/api/',
		tokenName: 'Navace-Token',
		interval: 30000
	}
});