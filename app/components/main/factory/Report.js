 'use strict';
app.factory('Report', function(Api, DSCacheFactory, $q, $rootScope, $interval, $filter, Settings){
	var $reportService = Api({cache: false, type: 'report'});
	var inProgress = false;

	var $report = {
		add: function(report){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstReports');

			report.timestamp = Date.now();

            var addReport = function(){
                if(!lst){ lst = []; }
                report.id = Math.uuid();
                (lst)?lst.push(report):lst = [report];
                DSCacheFactory.get($rootScope.hash).put('lstReports', lst);
            }

			navigator.geolocation
			.getCurrentPosition(function(position){
				report.latitude = position.coords.latitude;
				report.longitude = position.coords.longitude;
                addReport();
				defer.resolve();
			}, function(obj){
                addReport()
				defer.resolve();
                }, { maximumAge: 5000, timeout: 5000, enableHighAccuracy: true });

			return defer.promise;
		},
		get: function(filter){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstReports');
			if(filter){
				lst = $filter('filter')(lst, filter);
			}
			if(!lst || lst.length == 0){
				defer.reject([]);
				
			}else{
				defer.resolve(lst);
			}
			return defer.promise;
		},
		destroy: function(){
			DSCacheFactory.get($rootScope.hash).remove('lstReports');
		},
		save: function(){
			var defer = $q.defer();
			$report
			.get(function(elem){
					return !elem.submitted;
			 })
			.then(function(lst){
				if(lst.length==0 || inProgress){
                    defer.resolve();
                    return;
                }
				$reportService.save( 
					{
						reports: lst
					}, 
					function(obj){

							$report
							.get()
							.then(function(lst2){
                                angular.forEach(lst, function(elem){
                                    angular.forEach(lst2, function(elem2){
                                        if(elem.id == elem2.id){
                                            elem2.submitted = true;
                                        }
                                    });
				            });
                            inProgress = false;
                            DSCacheFactory.get($rootScope.hash).put('lstReports', lst2);
                            defer.resolve(obj);
						});
					}, 
					function(obj){
						console.warn('Unable to send report.', obj);
						inProgress = false;
						defer.reject(obj);
				});
			});
				
			return defer.promise;
		}
	}
	var interval = $interval(function(){
		$report.save();
	}, Settings.interval);
	$report.interval = interval;
	return $report;
});

