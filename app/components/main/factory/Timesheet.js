 'use strict';
app.factory('Timesheet', function(Api, DSCacheFactory, $q, $rootScope, $interval, $filter, Settings){
	var $timesheetService = Api({cache: false, type: 'timesheet'});
	var inProgress = false;
	var $timesheet = {
		add: function(timesheet){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstTimesheets');
			
			if(!timesheet.timestamp){
				timesheet.timestamp = Date.now();
			}
				if(!lst){ lst = []; }
				if(!timesheet.id){
					timesheet.id = Math.uuid();
					(lst)?lst.push(timesheet):lst = [timesheet];
				}else{
					var item = $filter('filter')(lst, function(elem){
						return elem.id == timesheet.id;
					});
					if(item && item.length>0){
						lst[lst.indexOf(item[0])] = timesheet;
					}else{
						(lst)?lst.push(timesheet):lst = [timesheet];
					}
				}
				DSCacheFactory.get($rootScope.hash).put('lstTimesheets', lst);
				defer.resolve();
			

			return defer.promise;
		},
		get: function(filter){
		
			var defer = $q.defer();
			var lst = DSCacheFactory.get($rootScope.hash).get('lstTimesheets');
			if(filter){
				lst = $filter('filter')(lst, filter);
			}
			if(!lst || lst.length == 0){
				defer.resolve([]);
				
			}else{
				defer.resolve(lst);
			}
			return defer.promise;
		},
		destroy: function(){
			DSCacheFactory.get($rootScope.hash).remove('lstTimesheets');
		},
		save: function(all){

			var defer = $q.defer();
			$timesheet
			.get(function(elem){
					return !elem.submitted && (elem.toSubmit||all);
			 })
			.then(function(lst){
				if(lst.length==0 || inProgress){ defer.resolve();return; }
				inProgress = true;
				$timesheetService.save( 
					{
						timesheets: lst
					}, 
					function(obj){
							$timesheet
							.get()
							.then(function(lst2){
                                angular.forEach(lst, function(elem){
                                    angular.forEach(lst2, function(elem2){
                                        if(elem.id == elem2.id){
                                            elem2.submitted = true;
                                        }
                                    });
							});
                            inProgress = false;
				            DSCacheFactory.get($rootScope.hash).put('lstTimesheets', lst2);
                            defer.resolve(obj);
						});
					}, 
					function(obj){
						console.warn('Unable to send timesheet.', obj);
						inProgress = false;
						defer.reject(obj);
				});
			});
				
			return defer.promise;
		}
	}
	var interval = $interval(function(){
		$timesheet.save();
	}, Settings.interval);
	$timesheet.interval = interval;
	return $timesheet;
});

