'use strict';
app.factory('Auth', function(Api, DSCacheFactory, $q, $rootScope, Settings) {
	var auth = Api({cache: false});
	var _Auth = DSCacheFactory.get('auth');
	var service = {
        users: function() {
            var lst = [];
            angular.forEach(_Auth.keys(), function(key){
                if(key != 'activated'){
                    var data = _Auth.get(key);
                    data.hash = key;
                    if(data.firstname){
                        data.label = data.firstname + ' ' + data.lastname;
                    }else{
                        data.label = data.username;
                    }
                    lst.push(data);
                }
            });
            return lst;
        },
        getLastLogin: function() {
            return DSCacheFactory.get('data').get('lastLogin');
        },
        currentUser: function(user) {
            if(user != null){
                _Auth.put($rootScope.hash, user);
            }
            return _Auth.get($rootScope.hash);
        },
		login: function(id, hash) {
            var defer = $q.defer();
            var successLogin = function() {
                service.createCache(id, hash);
                DSCacheFactory.get('data').put('lastLogin', hash);
                defer.resolve();
            }

            auth.get(
                { type: 'auth', id: id, token: hash },
                null,
                function(data) {
                    if(data.token == null){
                        defer.reject();
                        return;
                    }
                    successLogin();
                },
                function() {
                    if(_Auth.get(hash) != null){
                        successLogin();
                    }else{
                        defer.reject();
                    }
            });
			
			return defer.promise;
		},
        createCache: function(id, hash) {
            $rootScope.hash = hash;
            if( DSCacheFactory.get(hash) == null ){
                var cache = DSCacheFactory(hash, {
                    deleteOnExpire: 'aggressive',
                    storageMode: 'localStorage'
                });
                angular.forEach(_Auth.keys(), function(key){
                    if(key != 'activated'){
                        if(_Auth.get(key).id == id && key != hash) {
                            var oldCache;
                            if(key == 'authorizationData'){
                                oldCache = DSCacheFactory.get('defaultCache')
                                var t = oldCache.get('lstTimesheets');
                                var c = oldCache.get('lstChecksheets');
                                var r = oldCache.get('lstReports');

                                cache.put('lstTimesheets', t);
                                cache.put('lstChecksheets', c);
                                cache.put('lstReports', r);

                                oldCache.destroy();
                            }
                            else{
                                oldCache = DSCacheFactory.get(key);
                                angular.forEach(oldCache.keys(), function(key2){
                                    cache.put(key2, oldCache.get(key2));
                                });
                                DSCacheFactory.get(key).destroy();
                            }
                            _Auth.put(hash, _Auth.get(key));
                            _Auth.remove(key);
                        }
                    }
                });
            }
        },
		activate: function(id, login, password, autofill, firstname, lastname, email){
			var defer = $q.defer();
			var hash = CryptoJS.SHA512(login + password).toString();
			
			var _rejection = function(rejection){
				defer.reject(rejection);
			};
			 var _success = function(obj){
				if(!obj.token){
					defer.reject(obj);
					return false;
				}
                 service.createCache(id, hash);
				_Auth.put(hash, { token: obj.token, username: login, id: id, autofill: autofill, firstname: firstname, lastname: lastname, email: email });
				DSCacheFactory.get('data').put('lastLogin', hash);
				defer.resolve();
			}
			var macdefer = $q.defer();
			try{
				window.MacAddress.getMacAddress(function(macAddress) {
					macdefer.resolve(macAddress);
				});
			}catch(ex){
				macdefer.resolve(new Date().getMilliseconds());
			}
			macdefer.promise.then(function(mac){
			    auth.get({type: 'auth', id: id, token: hash, firstname: firstname, lastname: lastname, email: email, mac: mac, version: $rootScope.version||'5.0' }, null, _success, _rejection);
			});
			return defer.promise;
		},
		logout: function(){
			$rootScope.hash = null;
			return;
		}
	}
    return service;
});
app.factory('authInterceptorFactory', function ($q, $location, DSCacheFactory, Settings, $rootScope) {
	return {
		request: function (config) {
			config.headers = config.headers || {};
			var $auth = DSCacheFactory.get('auth');
			var authData = $auth && $rootScope.hash ? $auth.get($rootScope.hash): null;
			if (authData) {
				config.headers[Settings.tokenName] = authData.token;
			}
			return config;
		},
		responseError: function (rejection) {
			if (rejection.status === 401) {
				$location.path('/');
			}
			return $q.reject(rejection);
		}
	}
});