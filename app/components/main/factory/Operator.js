'use strict';
app.factory('Operator', function(Api, DSCacheFactory, $q, $rootScope, $timeout){
	var $operatorService = Api({cache: false});
	var $operator = {
		get: function(){
			var defer = $q.defer();
			var operator = DSCacheFactory.get($rootScope.hash).get('current');
			if(operator){
				defer.resolve(operator);
			}
			$operatorService.get({type: 'operator'}, null, function(obj){
			
				angular.forEach(obj.MachineSchedules, function(elem){
					if(elem.ScheduleDate){
						var date = elem.ScheduleDate.split('/')
						elem.ScheduleDate = new Date(date[0], date[1]-1, date[2]);
					}
				});
				angular.forEach(obj.Jobs, function(elem){
					if(elem.ScheduleDate){
						var date = elem.ScheduleDate.split(' ')[0].split('/');
						elem.ScheduleDate = new Date(date[2], date[1]-1, date[0]);
					}
				});
				DSCacheFactory.get($rootScope.hash).put('current', obj);
				defer.resolve(obj);
			}, function(){
				if(operator){
					defer.resolve(operator);
				}else{
					defer.reject();
				}
			});
			return defer.promise;
		}
	}
	return $operator;
});

