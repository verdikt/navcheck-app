app.factory('Api', function($resource, Settings){
	return function(optionsIn, params){
		var options = { 
				cache: true,
				type: ':type'
			}
		options = angular.extend(options, optionsIn);
		
		
		return $resource( Settings.apiEndpoint + options.type + '/:id', {},	{
			query: {isArray: false, method: 'GET', cache: options.cache},
            get: {method: 'GET', cache: options.cache},
            post: {method: 'POST', cache: options.cache},
			save: {method: 'POST', cache: options.cache}
		});
	}
})

