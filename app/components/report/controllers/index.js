'use strict';
app.controller('ReportCtrl', function($scope, DSCacheFactory, $rootScope){
	$scope.$cache = DSCacheFactory.get($rootScope.hash);

	$scope.report = {
		site: $scope.$cache.get('defaultSite'),
		machine: $scope.$cache.get('defaultMachine')
	};	
});
app.controller('ReportSiteSelectionCtrl', function($scope, $state, $filter, $rootScope){
	$scope.sites = $filter('unique')($rootScope.operator.MasterSites, 'MasterSiteId');
});

app.controller('ReportMachineSelectionCtrl', function($scope, $state, $filter, $rootScope){

	$scope.machines = $filter('unique')($rootScope.operator.Machines);
});

app.controller('ReportCategorySelectionCtrl', function($scope, $state, $filter, $rootScope){
	$scope.select = function(val){
		if(val){
			if(val == "CriticalIncident"){
				$state.go('report.severity');
			}else{
				$state.go('report.report');
			}
		}
	};
});
app.controller('ReportSeveritySelectionCtrl', function($scope, $state, $filter, $rootScope){
	
});
app.controller('ReportReportCtrl', function($scope, $state, $filter, $rootScope){
	$scope.getPhoto = function(imgData){
		($scope.report.pics)? $scope.report.pics.push(imgData): [imgData];
	}
    
    $scope.$watch('report.subcategory', function(){
        try{
        $scope.report.componentCode = $filter('filter')($filter('filter')($rootScope.operator.IncidentReport,{Name: $scope.report.criticity})[0].SubCategories, {Name: $scope.report.subcategory})[0].ComponentCode;
        }catch(ex){
            $scope.report.componentCode = null;
            console.warn('No componentCode', ex);
        }
    });
	
	
});
app.controller('ReportSubmitCtrl', function($scope, $state, Report, $timeout){
		$scope.submit = function(){	
			$scope.processing = true;
			$scope.message = navigator.onLine ? 'Submitting report, please wait...': 'You are now offline, submission will occur once you are back online.';
			Report.add($scope.report)
			.then(function(){
				Report.save().then(function(){
			
				}).finally(function(){
					$scope.processing = false;
					$state.go('menu');
				});
			});
		}
});

app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
	.state('report', {
		abstract: true,
		url: '/report',
		templateUrl: 'components/report/views/index.html',
	})
	.state('report.site', {
		url: '/site',
		templateUrl: 'components/report/views/site-selection.html',
	})
	.state('report.machine', {
		url: '/machine',
		templateUrl: 'components/report/views/machine-selection.html',
	})
	.state('report.category', {
		url: '/category',
		templateUrl: 'components/report/views/category.html',
	})
	.state('report.severity', {
		url: '/severity',
		templateUrl: 'components/report/views/severity.html',
	})
	.state('report.report', {
		url: '/report',
		templateUrl: 'components/report/views/report.html',
	})
	.state('report.submit', {
		url: '/submit',
		templateUrl: 'components/report/views/submit.html',
	});
});