'use strict';

describe('angularjs homepage', function() {
  it('should have a title', function() {
    browser.get('http://navcheck-app.deltadesire.com/');

    expect(browser.getTitle()).toEqual('NavCheck');
  });
});