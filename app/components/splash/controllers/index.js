'use strict';
app.controller('SplashCtrl', function(Auth, $scope, $state, $timeout, DSCacheFactory, $rootScope){
    
	$scope.users = Auth.users();
    var hash = Auth.getLastLogin();
    angular.forEach($scope.users, function(user){
        if(user.hash == hash){
            $scope.user = user;
            return;
        }
    });
    var i = 0;
    $scope.$watch('user', function(val) {
        if( i > 0 && val.autofill ) {
            $scope.login(val.id, val.hash);
        }
        i++;
    });



    $scope.loginCred = function(id, username, password) {

        $scope.login(id, CryptoJS.SHA512(username + password).toString());
    };

	$scope.login = function(id, hash){
		$scope.error = '';
		Auth.login(id, hash)
            .then(function() {
			    $state.go('menu');
		    }, function() {
                $scope.user.autofill = false;
			    $scope.error = 'Please check your information or reactivate your profile';
		    });
	};
    $scope.$watch(function(){
       return $rootScope.version;
    }, function(val){
        $scope.version = val;
        var $cache = DSCacheFactory.get('defaultCache');
        $cache.put('version', val);

    });

	
});
app.controller('MenuCtrl', function(Operator, Auth, $scope, $rootScope, $state, Checksheet, Report) {
	$scope.signOut = function() {
		$state.go('splash');
	}
	Operator.get().then(function(obj) {
		$rootScope.operator = obj;
	}, function(){
		$state.go('splash');
	});
});

app.controller('ActivateCtrl', function(Auth, $scope, $resource, Settings, $state){

	$scope.autofill = true;
	$scope.activate = function() {
		$scope.error = '';
		Auth
		    .activate($scope.operatorId, $scope.navaceUserName, $scope.navacePassword, $scope.autofill, $scope.firstname, $scope.lastname, $scope.email)
		    .then(function(obj) {
			$state.go('menu');
		}, function(obj){
			console.warn(obj);
			$scope.error = 'Please check your information';
			$scope.navaceUserName = '';
			$scope.navacePassword = '';
			$scope.tou = false;
		});
	}
});

app.controller('ProfileCtrl', function($scope, Auth, $state, $rootScope, DSCacheFactory) {
    $scope.user = Auth.currentUser();

    $scope.save = function(){
        Auth.currentUser($scope.user);
        $state.go('menu')
    }
	$scope.destroy = function(){
		if(confirm('Are you sure to delete your profile?')){
			DSCacheFactory.get($rootScope.hash).destroy();
			DSCacheFactory.get('auth').remove($rootScope.hash);
			Auth.logout();
			$state.go('splash');
		}
	};
});

app.config(function($stateProvider) {
	  $stateProvider
		.state('splash', {
		  url: '/',
		  templateUrl: 'components/splash/views/index.html'
		})
		.state('menu', {
		  url: '/menu',
		  templateUrl: 'components/splash/views/menu.html'
		})
		.state('activate', {
			url: '/activate',
			templateUrl: 'components/splash/views/activate.html'
		})
		.state('edit-profile', {
			url: '/edit-profile',
			templateUrl: 'components/splash/views/profile.html'
		})
	});