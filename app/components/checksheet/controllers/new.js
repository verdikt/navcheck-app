'use strict';
app.controller('ChecksheetNewCtrl', function($scope, DSCacheFactory, $rootScope){

	var $cache = DSCacheFactory.get($rootScope.hash);
	
	$scope.checksheet = {
		site: $cache.get('defaultSite'),
		machine: $cache.get('defaultMachine')
	};
	/*$scope.$watchCollection('checksheet', function(val){
		if(val){
			$cache.put('checksheet', val);
		}
	});*/
	
});

app.controller('ChecksheetNewSiteSelectionCtrl', function($scope, $state, $filter, $rootScope){

	$scope.sites = $filter('unique')($rootScope.operator.MasterSites, 'MasterSiteId');
});

app.controller('ChecksheetNewOperatorDeclarationCtrl', function($scope, $state, Checksheet, $ionicScrollDelegate){

	$scope.disagree = function(){
		$scope.checksheet.operatorOk = false;
		Checksheet.add($scope.checksheet);		
		alert('Please report to your Supervisor that you are unfit for work.');

		$state.go('menu');
	}
	
	$scope.ok = function(){
		$scope.checksheet.operatorOk = new Date();
		$state.go('checksheet.machine');
        navigator.geolocation.getCurrentPosition(function(position){
            $scope.checksheet.latitude = position.coords.latitude;
            $scope.checksheet.longitude = position.coords.longitude;
        }, function(obj){
            console.error(obj);
        }, { enableHighAccuracy: true });	
	}
	
	$ionicScrollDelegate.resize();
});

app.controller('ChecksheetNewMachineSelectionCtrl', function($scope, $state, $rootScope, $filter){

});

app.controller('ChecksheetNewSmuEntryCtrl', function($scope, $state, $rootScope, $filter){
	
	$scope.optional = false;
});


app.controller('ChecksheetNewScheduleCtrl', function($scope, $state, $rootScope, $filter, $ionicModal){

	$scope.evs = $filter('filter')($rootScope.operator.MachineSchedules, {'MachineId' : $scope.checksheet.machine?$scope.checksheet.machine.MachineId:0});
	angular.forEach($scope.evs, function(elem){
		if( typeof(elem.ScheduleDate) != 'object' ){
			elem.ScheduleDate = moment(elem.ScheduleDate, 'YYYY-MM-DD').toDate();
		}
		elem.duration = moment.duration(moment(elem.ScheduleDate) - moment());
	});
	$ionicModal.fromTemplateUrl('components/checksheet/views/new/schedule-info.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});
	$scope.sortDate = function(el){
		var date = moment(el.ScheduleDate);
		return date.format();
	}
	$scope.info = function(ev){
		$scope.ev = ev;
		$scope.modal.show();
	}
	
});

app.controller('ChecksheetNewJobsCtrl', function($scope, $state, $rootScope, $filter, $ionicModal){
	
	var lstJobs = $filter('unique')($rootScope.operator.Jobs);
	$scope.jobs = [];
	$scope.checksheet.jobs = [];
	angular.forEach(lstJobs, function(job){
		if(job.MachineId.toString() == ( $scope.checksheet.machine ? $scope.checksheet.machine.MachineId.toString() : '' )){
            
            if(job.Priority == 'Urgent'){
				$scope.outofservice = true;
			}
            if(!job.Priority){
				job.Priority = 'When Required';
			}
            switch(job.Priority){
                case "ASAP":
                    job.weight = 2;
                break;
                case "Breakdown":
                    job.weight = 1;
                break;
                case 'Next Service':
                    job.weight = 0;
                break;
                case 'When Required':
                    job.weight = -1;
                break;
                default:
                    job.weight = -2;
                break;
            }
			
			
			$scope.checksheet.jobs.push(job);
		}
	});
	$scope.joblst = $filter('orderBy')(
								$filter('orderBy')($scope.checksheet.jobs, 'InputDate', false), 
								'weight', true);
    
	$ionicModal.fromTemplateUrl('components/checksheet/views/new/job-info.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});
	$scope.info = function(ev){
		$scope.ev = ev;
		$scope.modal.show();
	}
	$scope.endCheck = function(){};
});

app.controller('ChecksheetNewItemsCtrl', function($scope, $state, $rootScope, $filter, $ionicScrollDelegate){

	$scope.item = $scope.checksheet.items[$state.params.id-1];
    $ionicScrollDelegate.resize();


	$scope.check = function(){
		$scope.$$postDigest(function(){
			if($scope.itemForm.$valid){
				$scope.next();
			}
		});

	};
	$scope.$watch(function() { return $scope.itemForm.$valid; }, function(){
	
		if(!$scope.item){ return false; }
		
        if(!$scope.item.timestamp){
            $scope.item.timestamp = Date.now();
        }
		if($scope.itemForm.$dirty){
			$scope.item.timestamp = Date.now();
			navigator.geolocation.getCurrentPosition(function(position){
				$scope.item.latitude = position.coords.latitude;
				$scope.item.longitude = position.coords.longitude;
			}, function(obj){
				console.error(obj);
			}, { maximumAge: 5000, timeout: 5000, enableHighAccuracy: true });
		}
	});
    angular.forEach($scope.checksheet.items, function(elem){
        var st = parseInt(elem.status);
        if(st >= 4 && st <= 6 ){
            $scope.checksheet.MachineOk = false;
        }
    });
	/*$scope.back = function(){
		if($state.params.id-1 == 0){
			$state.go('checksheet.jobs');
		}else{
			$state.go('checksheet.items.item', {id: parseInt($state.params.id)-1})
		}
	};*/
	
	$scope.next = function(){
		if($state.params.id < $scope.checksheet.items.length){
			$state.go('checksheet.items.item', {id: parseInt($state.params.id)+1})
		}else{
			$state.go('checksheet.submission');
		}
	};
});
app.controller('ChecksheetNewSubmissionCtrl', function($scope, Checksheet, $timeout, $state, Timesheet, $rootScope){
	$scope.photoCount = 0;
	angular.forEach($scope.checksheet.items, function(elem){
		$scope.photoCount += (elem.pics!=null) ? elem.pics.length : 0;
	});
	$scope.submit = function(){
		$scope.processing = true;
		$scope.message = '';
		var date = new Date();
		var timesheet = {
				TS_Machine: $scope.checksheet.machine,
				TS_Site: $scope.checksheet.site,
				TS_Start: moment($scope.checksheet.operatorOk),
				TS_Finish: moment(date),
				TS_Supervisor: null,
				readonly: true
			};
		
				angular.forEach($rootScope.operator.TimesheetSupervisors, function(elem){
					if(elem.MasterSiteID.toString() == $scope.checksheet.site.MasterSiteId.toString()){
						timesheet.TS_Supervisor = elem;
						return;
					}
				});
		Timesheet.add(timesheet);
		$scope.submittimg = true;
		Checksheet.add($scope.checksheet)
		.then(function(){
			$scope.message = navigator.onLine ? 'Submitting Checksheet, please wait...': 'You are now offline, submission will occur once you are back online.';
			Checksheet.save();
		})
		.finally(function(){
			$timeout(function(){
				$scope.processing = false;
				$state.go('menu');
			}, 4000);
		});
	}
});



app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
	.state('checksheet', {
		url: '/checksheet',
		abstract: true,
		templateUrl: 'components/checksheet/views/new/index.html',
	})
	.state('checksheet.site', {
		url: '/site',
		templateUrl: 'components/checksheet/views/new/site-selection.html',
	})
	.state('checksheet.operator', {
		url: '/operator',
		templateUrl: 'components/checksheet/views/new/operator-declaration.html',
	})
	.state('checksheet.machine', {
		url: '/machine',
		templateUrl: 'components/checksheet/views/new/machine-selection.html',
	})
	.state('checksheet.smu', {
		url: '/smu',
		templateUrl: 'components/checksheet/views/new/smu-entry.html',
	})
	.state('checksheet.jobs', {
		url: '/jobs',
		templateUrl: 'components/checksheet/views/new/jobs.html',
	})
	.state('checksheet.schedule', {
		url: '/schedule',
		templateUrl: 'components/checksheet/views/new/schedule.html',
	})	
	.state('checksheet.items', {
		url: '/items',
		abstract: true,
		controller: function($scope, $filter, $rootScope){
            var today = new Date();
            $scope.checksheet.items = $filter('filter')($rootScope.operator.CheckListItems, function(elem){
                var isMachine = elem.MachineId.toString() == $scope.checksheet.machine.MachineId.toString();

                var isComponent = elem.ComponentCode == '7520' || ( elem.ComponentCode == '7521' && today.getDay() == 3 );
                return isMachine && isComponent;
            });
            $scope.checksheet.ComponentCode = today.getDay() == 3 ? 7521 : 7520
				angular.forEach($scope.checksheet.jobs, function(elem){
					var reported = 0;
					if(elem.ChecklistitemRef!=null){
						angular.forEach($scope.checksheet.items, function(elem2){
							if(elem2.ChecklistId == elem.ChecklistitemRef){
								var reportedTxt = (reported!=0)? 'Repair reported(' + reported + ')':'Repair Reported';
								elem2.reported = { 2: 'Adjusted', '3': 'Replaced', 4: 'Requires repair (Not Urgent)', 5: 'Environmental Issue', 6: 'Safety Issue', 7: reportedTxt };
								elem2.status = '7';
								if(!elem2.jobs){ elem2.jobs = []; }
                                elem2.jobs.push(elem);
								reported++;
							}
						});
					}
				});
		},
		template: '<ion-content scrollbar-y="false"><ion-nav-view animation="fade-in-out"></ion-nav-view></ion-content>',
	})
	.state('checksheet.items.item', {
		url: '/:id',
		templateUrl: 'components/checksheet/views/new/items.html',
	})
	.state('checksheet.submission', {
		url: '/submission',
		templateUrl: 'components/checksheet/views/new/submission.html',
	})		
});