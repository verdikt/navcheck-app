'use strict';
app.controller('ChecksheetPreviousCtrl', function($scope, DSCacheFactory){

});

app.controller('ChecksheetPreviousSummaryCtrl', function($scope, Checksheet, $state, $rootScope){

	Checksheet.get()
	.then(function(lst){
		$scope.lst = lst;
	});
	
	$scope.openChecksheet = function(chk){
		$scope.cpc.chk = chk;
		$state.go('previous-checksheet.machine-checksheet')
	}
});

app.controller('ChecksheetPreviousMachineChecksheetCtrl', function($rootScope, $scope, $state){
	
	$scope.openChecksheetDetails = function(chk, index){
		chk.index = index;
		$scope.cpc.item = chk;
		$state.go('previous-checksheet.details');
	}
});

app.controller('ChecksheetPreviousChecksheetDetailsCtrl', function($rootScope, $scope, $state, $filter){

	$scope.$watch('item.ChecklistitemRef',function(){
		if($scope.item)
			$scope.jobs = $filter('filter')($scope.operator.Jobs, {ChecklistitemRef: $scope.item.ChecklistId});
	});
	$scope.previous = function(){
		var index = $scope.item.index-1
		var item = $scope.cpc.chk.items[index];
		if(item){
			item.index = index;
			$scope.item = item;
		}
	}
	$scope.next = function(){
		var index = $scope.item.index+1
		var item = $scope.cpc.chk.items[index];
		if(item){
			item.index = index;
			$scope.item = item;
		}
	}
	$scope.history = window.history;
	$scope.item = $scope.cpc.item;
});



app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
	.state('previous-checksheet', {
		url: '/previous-checksheet',
		abstract: true,
		templateUrl: 'components/checksheet/views/previous/index.html',
	})
	.state('previous-checksheet.summary', {
		url: '/summary',
		templateUrl: 'components/checksheet/views/previous/summary.html',
	})
	.state('previous-checksheet.details', {
		url: '/details',
		templateUrl: 'components/checksheet/views/previous/details.html',
	})
	.state('previous-checksheet.machine-checksheet', {
		url: '/machine-checksheet',
		templateUrl: 'components/checksheet/views/previous/machine-checksheet.html',
	});
});