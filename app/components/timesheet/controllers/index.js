'use strict';
app.controller('TimesheetNewCtrl', function($scope, Timesheet){	


});

app.controller('TimesheetNewSiteSelectionCtrl', function($scope, $state, $filter, $rootScope){

	$scope.sites = $filter('unique')($rootScope.operator.MasterSites, 'MasterSiteId');

});
app.controller('TimesheetListCtrl', function($scope, $state, Timesheet, $filter){
	Timesheet.get(function(item){
				return !item.submitted;
	})
	.then(function(lst){
		var data = {};
		angular.forEach(lst, function(elem){
			var date = new Date(elem.TS_Start?elem.TS_Start:elem.TS_Finish);
			date = moment(date).format('dddd DD/MM/YYYY');
			data[date]?data[date].push(elem):data[date]=[elem];
		});
		$scope.timesheets = data;
	});
	
	$scope.edit = function(ts){
		if(ts.readonly==true){
			alert('This timesheet cannot be edited.');
			return false;
		}
		$state.go('timesheet.edit-entry', {id: ts.id});
	}
	$scope.submit = function(){
		$scope.processing = true;
		$scope.message = navigator.onLine ? 'Submitting timesheet, please wait...': 'You are now offline, submission will occur once you are back online.';
		Timesheet
		.save(true)
		.then(function(){
			Timesheet.save().finally(function(){
                $scope.processing = false;
				$state.go('menu');
			});
		}).finally(function(){

		});

	}
	$scope.duration = function(ts){
		if(ts.TS_Finish == null || ts.TS_Start == null){
			return '';
		}
		
		var min = Math.abs(moment.duration(moment(new Date(ts.TS_Finish)) - moment(new Date(ts.TS_Start))).asMinutes());
		var h = Math.floor(min/60);
		var time = '';
		if(h>0){ time += h + ' hrs '; }
		time += Math.ceil(min%60) + ' min';
		return time;
	}
	$scope.sortDate = function(date){
		var date = moment(date, 'DD/MM/YYYY hh:mm a');
		return date.toDate();
	}
});
app.controller('TimesheetNewTimeEntryCtrl', function($scope, $state, Timesheet, $filter, DSCacheFactory, $timeout, $rootScope, $q){
    var $cache = DSCacheFactory.get($rootScope.hash);

    //$scope.ts.TS_Site = $cache.get('defaultSite');
    //$scope.ts.TS_Machine = $cache.get('defaultMachine');

	$scope.rates = [{name: 'ST', value: 'ST'}, {name: 'OT', value: 'OT'}, {name: 'DT', value: 'DT'}];
	$scope.duration = function(){
		if(!$scope.ts.TS_Start || !$scope.ts.TS_Finish){
			return '';
		}
		var min = Math.abs(moment.duration($scope.ts.TS_Finish - $scope.ts.TS_Start).asMinutes());
		var h = Math.floor(min/60);
		var time = '';
		if(h>0){ time += h + ' hrs '; }
		time += Math.ceil(min%60) + ' min';
		return time;
	}
	
	var start = $('#start'),
	end = $('#end'),
	body = $('ion-content:first');
	$scope.dateoptions = { 
		theme: 'android',
        display: 'bottom',
        mode: 'scroller',
		dateFormat: 'dd/mm/yyyy', 
		timeFormat: 'hh:ii a',
		dateOrder : 'D ddM', 
		//context: null,
		buttons: [
			'cancel',
			'clear', 
			{ 
				text: 'Now',
				handler: function (event, inst) { 
					inst.setDate(new Date(), true);
					inst.buttons.set.handler();
				} 
			},
			'set'
		]
	};
	$scope.done = function(){
		$scope.message = "Saving...";
		(function(){
			var defer = $q.defer();
			Timesheet.add($scope.ts).then(function(){
				var last;
				if($state.params.id){
					last = $scope.timesheets[$scope.timesheets.indexOf($scope.ts)-1];
				}else{
					last = $scope.timesheets[$scope.timesheets.length-1];
				}
				if(last && last.TS_Finish==null){	
					last.TS_Finish = $scope.ts.TS_Start;
					Timesheet.add(last).finally(function(){
						defer.resolve();
					});
				}else{
					defer.resolve();
				}		
			});
			return defer.promise;
		})().then(function(){
			$state.go('timesheet.list');
		});
	}
		//$scope.$on('loaded', function(){
		Timesheet.get(function(item){
			return item.submitted != true;
		})
		.then(function(lst){
			$scope.timesheets =  $filter('orderBy')(lst, function(item){
				return new Date(item.TS_Start);
			});
			$scope.$broadcast('loaded');
			if($state.params.id){
				var tmc = $filter('filter')($scope.timesheets, function(elem){
					return elem.id == $state.params.id;
				});
				
				if(tmc.length!=1){ $state.go('timesheet.list'); }
				
				$scope.ts = tmc[0];
				$scope.index = $scope.timesheets.indexOf($scope.ts);
				$scope.dateoptions.disabled = $scope.ts.readonly;
				$scope.ts.TS_Finish = $scope.ts.TS_Finish?moment(new Date($scope.ts.TS_Finish)):null;
				$scope.ts.TS_Start = $scope.ts.TS_Start?moment(new Date($scope.ts.TS_Start)):null;

			}else{
				var site = $cache.get('defaultSite'),
				machine = $cache.get('defaultMachine'),
				supervisor = null;
				if(site){
					angular.forEach($rootScope.operator.TimesheetSupervisors, function(elem){
						if(elem.MasterSiteID.toString() == site.MasterSiteId.toString()){
							supervisor = elem;
							return;
						}
					})
				}
				$scope.index = $scope.timesheets.length;
				$scope.ts = {
					//TS_Start: moment(),
					//TS_Finish: moment(),
					TS_Rate: $scope.rates[0].name,
					TS_Site: site,
					TS_Machine: machine,
					TS_Supervisor: supervisor
				}
				navigator.geolocation.getCurrentPosition(function(position){
					$scope.ts.latitude = position.coords.latitude;
					$scope.ts.longitude = position.coords.longitude;
				}, function(obj){
					console.error(obj);
				}, { enableHighAccuracy: true });	
				var last = $scope.timesheets[$scope.timesheets.length-1];
				if(last){	
					if(last.TS_Finish != null){
						$scope.ts.TS_Start = last.TS_Finish;
					}
				}
			}
			$scope.fromSite = function(elem){
				if(!$scope.ts.TS_Site || !$scope.ts.TS_Site.MasterSiteId){return false;}
				return elem.MasterSiteID.toString() == $scope.ts.TS_Site.MasterSiteId.toString();
			}
		}).finally(function(){
			var opt = angular.extend(angular.copy($scope.dateoptions), { 
				context: body, 
				onSelect: function(value){
					$scope.ts.TS_Start = moment(value, 'DD/MM/YYYY hh:mm a');
					$scope.ts.TS_Finish = null;
					$scope.$apply();
				},
				onClose: function(d,btn,g,h){
					if(btn == 'clear'){
						$scope.ts.TS_Start = null;
						$scope.$apply();
					}
				}
			});
			start.mobiscroll().datetime(opt);
			opt = angular.extend(angular.copy($scope.dateoptions), { 
				context: body, 
				onSelect: function(value){
					$scope.ts.TS_Finish = moment(value, 'DD/MM/YYYY hh:mm a')
					$scope.$apply();
				},
				onClose: function(d,btn,g,h){
					if(btn == 'clear'){
						$scope.ts.TS_Finish = null;
						$scope.$apply();
					}
				}
			});
			end.mobiscroll().datetime(opt);
			
					if($scope.ts.TS_Start!=null)
						$('#start').mobiscroll('setDate', $scope.ts.TS_Start.toDate(), true);
					if($scope.ts.TS_Finish!=null)
						$('#end').mobiscroll('setDate', $scope.ts.TS_Finish.toDate(), true);
		});
		$scope.$watch(function(){
			return $scope.ts.TS_Activity;
		}, function(val){
			$scope.readonlyMachine = false;
			if(val && (val == "Break" ||  val == "Travel" || val == "Toolbax Talk" || val == "Meeting" || val == "Induction")){
				$scope.ts.TS_Machine = null;
				$scope.readonlyMachine = true;
			}
		});
	
	$scope.open = function(id){
		$('#' + id).mobiscroll('open');
	}
	
	$scope.previous = function(){
		
		if($scope.index > 0){
			$scope.index--;
		}else{
			return false;
		}
		
		$state.go('timesheet.edit-entry', {id: $scope.timesheets[$scope.index].id});
	}
	
	$scope.next = function(){

		if($scope.index < $scope.timesheets.length - 1){
			$scope.index++;
		}else{
			return false;
		}
		
		$state.go('timesheet.edit-entry', {id: $scope.timesheets[$scope.index].id})
	}
});
app.controller('TimesheetSubmittedCtrl', function(){	

});
app.controller('TimesheetSubmittedCalendarCtrl', function($scope, $timeout, $state, $filter, Timesheet){

	$scope.duration = function(ts){
		var min = Math.abs(moment.duration(moment(ts.TS_Finish, 'YYYY-MM-DD hh:mm a')-moment(ts.TS_Start, 'YYYY-MM-DD hh:mm a')).asMinutes());
		var h = Math.floor(min/60);
		var time = '';
		if(h>0){ time += h + ' hrs '; }
		time += Math.ceil(min%60) + ' min';
		return time;
	}
	
	var calendar = $('#calendar'),
		events = [], durations = [];

		Timesheet
		.get(function(elem){
			return elem.submitted;
		})
		.then(function(lst){
			$scope.timesheets = lst;
			angular.forEach(lst , function(elem){
				
				var separator = ' - ',
				task = ((elem.TS_Task!=null)?'Task: ' + elem.TS_Task:'No task') + separator,
				activity = (elem.TS_Activity!=null)?('Activity: ' + elem.TS_Activity ):'No activity' + separator,
				start = (elem.TS_Start!=null)?'' + moment(new Date(elem.TS_Start)).format('hh:mm a') + separator :'',
				finish = (elem.TS_Finish!=null)?'' + moment(new Date(elem.TS_Finish)).format('hh:mm a') + separator :'',
				description = (elem.TS_Description!=null)?'Description: '+ elem.TS_Description:'',
                min = 0;
                
                if(elem.TS_Start!=null && elem.TS_Finish!=null){
                    min = Math.abs(moment.duration(moment(elem.TS_Finish, 'YYYY-MM-DD hh:mm a')-moment(elem.TS_Start, 'YYYY-MM-DD hh:mm a')).asMinutes());
                    var date = moment(elem.TS_Start).toDate();
                    if(!durations[date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate()]){
                        durations[date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate()]=0;
                    }
                    durations[date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate()]+=min;
                }
                
				events.push({ 
					start: elem.TS_Start?new Date(elem.TS_Start):new Date(elem.TS_Finish),
					end: new Date(elem.TS_Finish),
					text: finish + task + activity + description + separator + $scope.duration(elem),
					//icon: 'clock',
					id: elem.id
				});
			});
			showUp();
	});
	$scope.$on('$destroy', function() {
		calendar.mobiscroll('destroy');
	})
	var showUp = function(){
		$timeout(function(){
			calendar.mobiscroll()
			.calendar({ 
				//layout: 'liquid',
				display: 'bottom', 
				context: calendar.parent(),
				//mode: 'mixed', 
				buttons: [
					//'set',
					//'cancel'
				],
				events: events,
				markedText: true,
				onEventSelect: function(e, event, d, inst){
					console.log(e, event, d , inst)
					//alert($('.dw-cal-event-dur').html())
					//$state.go('timesheet.edit-entry', {id: event.id});
				},
				//closeOnOverlay: false
			});
			//calendar.mobiscroll('setEvents', events)
			calendar.mobiscroll('show');
            
            var days  = $('.dw-cal-day');
            var start = new Date(0, 0, 0, 0, 0, 0, 0);
            angular.forEach(Object.keys(durations), function(key){
               days.each(function(i, ui){
                   if($(this).data('full')==key){
                    var minutes = durations[key];
                    var finish = new Date(0, 0, 0, 0, minutes, 0, 0);
                    $(this).find('.dw-cal-day-txt').html($scope.duration({TS_Finish: finish, TS_Start: start}));
                   }
               });
            });
			//console.log(events)
			//$scope.$apply();
		});
	}
	
	
});

app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
	.state('timesheet', {
		abstract: true,
		url: '/timesheet',
		templateUrl: 'components/timesheet/views/new/index.html',
	})
	.state('timesheet.site', {
		url: '/site',
		templateUrl: 'components/timesheet/views/new/site-selection.html',
	})
	.state('timesheet.list', {
		url: '/list',
		templateUrl: 'components/timesheet/views/new/list.html',
	})
	.state('timesheet.entry', {
		url: '/entry',
		templateUrl: 'components/timesheet/views/new/time-entry.html',
	})
	.state('timesheet.edit-entry', {
		url: '/entry/:id',
		templateUrl: 'components/timesheet/views/new/time-entry.html',
	})
	.state('timesheet-submitted', {
		abstract: true,
		url: '/timesheet-submitted',
		templateUrl: 'components/timesheet/views/submitted/index.html',
	})
	.state('timesheet-submitted.calendar', {
		url: '/calendar',
		templateUrl: 'components/timesheet/views/submitted/calendar.html',
	});
});